#!/usr/bin/env python

import csv
import glob
import os
import re
import sys
import pandas as pd 
import matplotlib
matplotlib.use('AGG')
import matplotlib.pyplot as plt 
pd.options.display.mpl_style = 'default'

from tika import parser

input_path = sys.argv[1]

def create_df(pdf_content, content_pattern, line_pattern, column_headings):

	list_of_line_items = []

	content_match = re.search(content_pattern, pdf_content, re.DOTALL)

	content_match = content_match.group(1)

	content_match =content_match.split('\n')

	for item in content_match:

		line_items = []

		line_match = re.search(line_pattern, item, re.I)

		agency = line_match.group(1).strip().replace(',', '')

		values_string = line.match.group(2).strip().replace('- ', '0.0 ').replace('$', '').replace(',', '')

		values = map(float, values_string.split())

		line_items.append(agency)

		line_items.extend(values)


		list_of_line_items.append(line_items)


	df = pd.DataFrame(list_of_line_items, columns=column_headings)

	return df




def create_plot(df, column_to_sort, x_val, y_val, type_of_plot, plot_size, the_title):

	fig, ax = plt.subplots()

	df = df.sort_values(by=column_to_sort)

	df.plot(ax=ax, x=x_val, y=y_val, kind=type_of_plot, figsize = plot_size, title=the_title)

	plt.tight_layout()

	pngfile = the_title.replace(' ', '_') + '.png'

	plt.savefig(pngfile)

	


