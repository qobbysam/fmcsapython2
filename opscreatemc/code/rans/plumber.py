
import os
import csv
import camelot
import pandas as pd 
import itertools
#import tabula


BASE_DIR = os.getcwd()

PDF_DIR = os.path.join(BASE_DIR,'pdfdata', 'apr', '')

JSON_DIR = os.path.join(BASE_DIR, 'jsondata', 'tests', '')

TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates', '')

FILE_LOCATION_PDF =  PDF_DIR + 'REGISTER20190402.pdf'

FILE_LOCATION_JSON = JSON_DIR + 'REGISTER20190402c.csv'

TEMPLATE_FILE = TEMPLATE_DIR + 'REGISTER20190402.tabula-template.json'



def main():

	tables = camelot.read_pdf(FILE_LOCATION_PDF,  flavor='stream', pages='all')

	count = 1
	#tables = cm.read_pdf(file, pages='all')
	df = pd.concat([tab.df for tab in tables], ignore_index=True)
#	df.to_csv(file.split('.')[0]+'_camelot.csv')
	df.to_csv(FILE_LOCATION_JSON)

	#combined_csv = list(itertools.chain.from_iterable(tables))

	# for table in tables:
	# 	count +=1
	# 	pd.concat(table[])

	# print(pm)

	#combined_csv = pd.concat([pd.read_csv(table) for table in tables ])


	#tables.to_csv(FILE_LOCATION_JSON)
	#combined_csv.to_csv(FILE_LOCATION_JSON, index=False, encoding='utf-8-sig')


	#tables.export(FILE_LOCATION_JSON, f='csv', compress=True)

	#print(combined_csv)





if __name__ == '__main__':
	main()