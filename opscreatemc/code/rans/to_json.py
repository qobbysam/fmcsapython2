import os
import csv
import pandas as pd 

import tabula


BASE_DIR = os.getcwd()

PDF_DIR = os.path.join(BASE_DIR,'pdfdata', 'apr', '')

JSON_DIR = os.path.join(BASE_DIR, 'jsondata', 'tests', '')

TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates', '')

FILE_LOCATION_PDF =  PDF_DIR + 'REGISTER20190402.pdf'

FILE_LOCATION_JSON = JSON_DIR + 'REGISTER20190402.csv'

TEMPLATE_FILE = TEMPLATE_DIR + 'REGISTER20190402.tabula-template.json'


def main():
	print (FILE_LOCATION_JSON)
	print(FILE_LOCATION_PDF)
	
	#tabula.convert_into(FILE_LOCATION_PDF, FILE_LOCATION_JSON, output_format="json", pages='all' , silent=True, multiple_tables= True)
	#dt  = tabula.read_pdf(FILE_LOCATION_PDF, pages='all' , multiple_tables= True)
	dt  = tabula.read_pdf_with_template(FILE_LOCATION_PDF, TEMPLATE_FILE)

	df = pd.DataFrame(dt)

	df.to_csv(FILE_LOCATION_JSON, encoding='utf-8')
	#print(dt)





if __name__ == '__main__':
	main()
