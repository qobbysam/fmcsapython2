import os

#import tabula

from tika import parser


BASE_DIR = os.getcwd()

PDF_DIR = os.path.join(BASE_DIR,'pdfdata', 'apr', '')

JSON_DIR = os.path.join(BASE_DIR, 'jsondata', 'apr', '')

FILE_LOCATION_PDF =  PDF_DIR + 'REGISTER20200401.pdf'

FILE_LOCATION_JSON = JSON_DIR + 'REGISTER20200401.json'


def main():
	print (FILE_LOCATION_JSON)
	print(FILE_LOCATION_PDF)
	
	#tabula.convert_into(FILE_LOCATION_PDF, FILE_LOCATION_JSON, output_format="json", pages='all' , silent=True, multiple_tables= True)
	# tabula.read_pdf(FILE_LOCATION_PDF, pages='all' , multiple_tables= True)

	parsed_pdf = parser.from_file(FILE_LOCATION_PDF)

	data = parsed_pdf['content']

	parsed_pdf.viewkeys()

	print (data)



if __name__ == '__main__':
	main()
