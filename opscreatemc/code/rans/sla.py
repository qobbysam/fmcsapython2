import os
import slate3k as slate
import tabula


BASE_DIR = os.getcwd()

PDF_DIR = os.path.join(BASE_DIR,'pdfdata', 'apr', '')

JSON_DIR = os.path.join(BASE_DIR, 'jsondata', 'apr', '')

FILE_LOCATION_PDF =  PDF_DIR + 'REGISTER20200401.pdf'

FILE_LOCATION_JSON = JSON_DIR + 'REGISTER20200401.txt'


def main():

	with open(FILE_LOCATION_PDF, 'rb') as f:
		extracted_text = slate.PDF(f)

		print (extracted_text)
	with open (FILE_LOCATION_JSON, 'w') as m:
		m.write(str(extracted_text))



if __name__ == '__main__':
	main()