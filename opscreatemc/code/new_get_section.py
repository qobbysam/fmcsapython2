import os
import csv
import itertools

import re


BASE_DIR = os.getcwd()

DATA_DIR = os.path.join(BASE_DIR, 'data', 'com')


#out_csv_filename_grant_decision = "grants_mar_7.csv"
gb_filename = "new_gb_1"
out_csv_filename_grants = "new_grants_1"

def get_files(month_path):

    fileslist = next(os.walk(month_path))[2]

    file_list_no_extension = []
    dates = []

    
    for filename in fileslist:
        new_name = filename.strip('.csv')

        file_list_no_extension.append(new_name)

    return file_list_no_extension


def get_start_end_lines(filename):





    in_csv_file = os.path.join(DATA_DIR, filename) + '.csv'

    name_changes_pattern = re.compile(r'\d+NAME\sCHANGES')

    grant_authority_pattern = re.compile(r'\d+CERTIFICATES,\sPERMITS')

    certicate_registration_pattern = re.compile(r'\d+CERTIFICATES\sOF\sREGISTRATION')

    #Decisions_patern = re.compile(r'\d+DECISIONS\sAND\sNOTICES')
    withdrawal_pattern = re.compile(r'\d+WITHDRAWAL')

    dismissal_pattern = re.compile(r'\d+DISMISSALS')

    revocation_pattern = re.compile(r'\d+REVOCATION\sACTIONS')



    info_out = {
            'name_change_start_line': 0,
            'grant_start_line': 0,
            'certificate_registration_start_line':0,
            'withdrawal_start_line': 0,
            'dismissal_start_line': 0,
            'revocation_start_line' : 0,

    }


    with open(in_csv_file, 'r', encoding="utf8") as cs_in:

        csv_file = csv.reader(cs_in, delimiter= ",")

        for key, line in enumerate(csv_file, 1):
            joined_cells = str(''.join(line))

            if name_changes_pattern.match(joined_cells):

                info_out['name_change_start_line'] = key

                print("found namechange at  {}".format(key))

            elif grant_authority_pattern.match(joined_cells):
                info_out['grant_start_line'] =key

                print("authority_FOOUN at {} ".format(key))



            elif certicate_registration_pattern.match(joined_cells):
                info_out['certificate_registration_start_line'] = key
                print("certicate_registration_pattern at {} ".format(key))

                
            elif withdrawal_pattern.match(joined_cells):
                info_out['withdrawal_start_line'] = key
                print("withdraw at {} ".format(key))


            elif dismissal_pattern.match(joined_cells):
                print("dismissal at {} ".format(key))

                info_out['dismissal_start_line'] = key 


            elif revocation_pattern.match(joined_cells):
                print("revocation at {} ".format(key))

                info_out['revocation_start_line'] = key

        return info_out


def save_grants(filename, start_line, end_line):

    in_csv_file = os.path.join(DATA_DIR, filename) + '.csv'

    grants_out_csv = os.path.join(BASE_DIR,'data','outcsv', out_csv_filename_grants ) 


    loop_start = start_line
    end_loop = end_line

    crap = []

    date_added = int(re.search(r'\d+', filename).group())

    def self_garbage(crap):

        

        garbage_file = os.path.join(BASE_DIR, 'data', 'gb', gb_filename )

        with open(garbage_file, 'a', encoding="utf8") as gb_file:

            csv_file = csv.writer(gb_file, delimiter=",")


            for line in crap:

                csv_file.writerow(line)
            
            crap = []

    def check_garbage(crap):

        if len(crap) % 50 == 0:

            self_garbage(crap)
        





    with open(in_csv_file, 'r', encoding="utf8") as cs_in ,open(grants_out_csv, 'a', encoding="utf8") as cs_out:

        csv_file = csv.reader(cs_in, delimiter=",")
        out_csv_file = csv.writer(cs_out, delimiter=",") 
        #name_change_line_pattern = re.compile(r'\w+-\d+\n')

        #pattern_1 = re.compile(r'\d+@+\w+-+\d+-+\w+\n')

        pattern_1 = re.compile(r'\d+@(\w)(\w)-\d+-+\w+\n+.')

        pattern_2 = re.compile(r'\d+@(\w)(\w)-\d+\n+.+@')

        pattern_3 = re.compile(r'\d+@(\w)(\w)-\d+@.+')

        pattern_4 = re.compile(r'\d+@(\w)(\w)-\d+-+(\w)@.+')

        pattern_5 = re.compile(r'')


        try:
            content = itertools.islice(csv_file, loop_start, end_loop)

        except Exception as e:

            print("numbers failed")
            print( e )

        for key, row in enumerate(content):

            joined_cells = str('@'.join(row))


            if "/" in joined_cells:


            

                if pattern_1.match(joined_cells):
                    
                    #print(joined_cells)
                    try:

                        list_to = pattern_1_handler(joined_cells)

                    except Exception as e:
                        print(e)
                        crap.append([joined_cells])

                    #split at first new line
                    list_to.append(date_added)

                    out_csv_file.writerow(list_to)

                    #get_mc_section

                    #check if has another \n

                    #merge and seperate texts
                elif pattern_2.match(joined_cells):

                    try:

                        list_to = pattern_2_handler(joined_cells)

                    except Exception as e:
                        print(e)

                        crap.append([joined_cells])
                    #split at first new line
                    list_to.append(date_added)

                    out_csv_file.writerow(list_to)

                elif pattern_3.match(joined_cells):

                    try:

                        list_to = pattern_3_handler(joined_cells)

                    except Exception as e:
                            print(e)
                            crap.append([joined_cells])
                        #split at first new line
                    list_to.append(date_added)

                    out_csv_file.writerow(list_to)


                elif pattern_4.match(joined_cells):
                    try:

                        list_to = pattern_4_handler(joined_cells)

                    except Exception as e:
                        print(e)
                        crap.append([joined_cells])
                    #split at first new line
                    list_to.append(date_added)

                    out_csv_file.writerow(list_to)

                else:
                    print (repr( joined_cells))

                    crap.append([joined_cells])

                    check_garbage(crap)

                    #pattern_3_handler(joined_cells)

            else:
                crap.append([joined_cells])

                check_garbage(crap)

        self_garbage(crap)


def pattern_1_handler(text_in):

    

    first_line_split = text_in.split("\n", 1)

    mc_section, other_section = first_line_split

    line_count, initials, number, type = mc_getter(mc_section)

    other_section_pre_clean = other_section


    #print(mc_section)

    #print(other_section)

    if "\n" in other_section:
        second_split = other_section.split("\n", 1)

        name_section , date_section = second_split

        new_date_string = '@'+date_section

        other_section_pre_clean = name_section + new_date_string

        # print(name_section)

        # print(date_section)

    
    other_section_clean = other_section_pre_clean.replace('@', '')

    date = other_section_clean[slice(-1,-11,-1)]

    date_reoddered = date[::-1]

    other_section_no_date = other_section_clean.replace(date_reoddered, '')

    #other_section_clean = other_section_clean.replace(date_reoddered, '')

    name,city,state,country = name_city_state(other_section_no_date)

    list_out = [initials,number,type,name,city,state,date_reoddered]

    print('%s , %s ,%s , %s ,%s , %s ,%s , %s ,%s  , pattern 1' %(line_count,date_reoddered,initials,number,type,name,city,state,country))
    
    return list_out

def pattern_2_handler(text_in):

    first_line_split = text_in.split("\n", 1)

    mc_section, other_section = first_line_split

    line_count, initials, number, type = mc_getter(mc_section)

    other_section_pre_clean = section_cleaner(other_section)
    
    # if "\n" in other_section:
    #     second_split = other_section.split("\n", 1)

    #     name_section , date_section = second_split

    #     new_date_string = '@'+date_section

    #     other_section_pre_clean = name_section + new_date_string

    

    date_reoddered = date_getter(other_section_pre_clean)

    other_section_clean = other_section.replace('@', '')

    other_section_no_date = other_section_clean.replace(date_reoddered, '')

    #other_section_clean = other_section_clean.replace(date_reoddered, '')

    name,city,state,country = name_city_state(other_section_no_date)

    #print ('%s  !!  %s !! %s  !!  %s' %(name,city,state,country))


    list_out = [initials,number,type,name,city,state,date_reoddered]

    print('%s , %s ,%s , %s ,%s , %s ,%s , %s ,%s  , pattern 2' %(line_count,date_reoddered,initials,number,type,name,city,state,country))
    
    return list_out

def pattern_3_handler(text_in):

    def clean_up(text_in):
        if "\n" in text_in:
            first_split = text_in.rsplit("\n",1)

            front,back = first_split

            new_back = '@'+back

            return front+new_back
        else:
            return text_in
    
    straight_text = clean_up(text_in)

    
    #print(straight_text)


    #split at second @

    section_spilt = "@".join(straight_text.split("@", 2)[:2])

    #section_spilt = straight_text.split('@', 2)

    #print(section_spilt)

    remain_text = straight_text.replace(section_spilt, '')

    clean_text = remain_text.replace('@', '')

    #print(clean_text)

    date_reoddered = date_getter(clean_text)

    clean_text_no_date = clean_text.replace(date_reoddered, '')

    #print(clean_text_no_date)

    line_count, initials, number, type = mc_getter(section_spilt)

    name,city,state,country = name_city_state(clean_text_no_date)
    list_out = [initials,number,name,type,city,state,date_reoddered]

    print('%s , %s ,%s , %s ,%s , %s ,%s , %s ,%s  , pattern 3' %(line_count,date_reoddered,initials,number,type,name,city,state,country))
    
    return list_out


    
def pattern_4_handler(text_in):
    def clean_up(text_in):
        if "\n" in text_in:
            first_split = text_in.rsplit("\n",1)

            front,back = first_split

            new_back = '@'+back

            return front+new_back
        else:
            return text_in
    
    straight_text = clean_up(text_in)


    #print(straight_text)


    #split at second @

    section_spilt = "@".join(straight_text.split("@", 2)[:2])

    #section_spilt = straight_text.split('@', 2)

    #print(section_spilt)

    remain_text = straight_text.replace(section_spilt, '')

    clean_text = remain_text.replace('@', '')

    #print(clean_text)

    date_reoddered = date_getter(clean_text)

    clean_text_no_date = clean_text.replace(date_reoddered, '')

    #print(clean_text_no_date)

    line_count, initials, number, type = mc_getter(section_spilt)

    name,city,state,country = name_city_state(clean_text_no_date)

    list_out = [initials,number,type,name,city,state,date_reoddered]

    print('%s , %s ,%s , %s ,%s , %s ,%s , %s ,%s  , pattern 4' %(line_count,date_reoddered,initials,number,type,name,city,state,country))
    
    return list_out




def date_getter(text_in):

    other_section_clean = text_in.replace('@', '')

    date = other_section_clean[slice(-1,-11,-1)]

    date_reoddered = date[::-1]

    return date_reoddered

def section_cleaner(text_in):

    if "\n" in text_in:
        second_split = other_section.split("\n", 1)

        name_section , date_section = second_split

        new_date_string = '@'+date_section

        other_section_pre_clean = name_section + new_date_string

        return other_section_pre_clean

    else:
        return text_in       

def mc_getter(text_in):
    #555@MC-889998-C
    #555@MC-889998

    clean = text_in.split('@')
    
    line_number,mc_number = clean

    count_dash = mc_number.count('-')

    if count_dash == 1:

        mc_number = mc_number.split('-')

        initials, number = mc_number

        type = 'C'

        return line_number,initials,number,type

    if count_dash == 2:
        mc_number_type = mc_number.split('-')

        initials,number,type = mc_number_type

        return line_number, initials,number,type

    else:

        print('failed to decode this MC % ' % (text_in))

    


def name_city_state(text_in):

    #name of company - city, state

    split_one = text_in.rsplit('-', 1)

    name, city_state = split_one

    counter_comma = city_state.count(',') 

    #print(city_state)

    

    if counter_comma == 1:

        city_state_us = city_state.split(',')

        city, state = city_state_us

        country = 'US'

        return name,city,state,country

    else:

        city_state_country = city_state.split(',')

        city, country, state = city_state_country


        return name,city,state,country

# this if else needs to be fixed for more than three commas






def main ():

	#get filelist



	filenames_list = get_files(DATA_DIR)


	for filename in filenames_list:

		# try:

		cur_file = get_start_end_lines(filename)
		name_change_start_line = cur_file['name_change_start_line']

		grant_start_line = cur_file['grant_start_line']
		certificate_registration_start_line = cur_file['certificate_registration_start_line']

		end_loop_line = cur_file['grant_start_line'] - 1



		#save_name_changes(filename, name_change_start_line, end_loop_line)


		end_loop_line = certificate_registration_start_line -1

		save_grants(filename, grant_start_line, end_loop_line)


