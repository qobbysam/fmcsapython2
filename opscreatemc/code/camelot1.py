




import os
#import csv
import camelot
import pandas as pd 
import itertools
import re

compile_string = re.compile('REGISTER2021.*')


#imports
MONTH_C = 'mar'

BASE_DIR = os.getcwd()

PDF_DIR = os.path.join(BASE_DIR,'pdfdata','data', '')

CSV_DIR = os.path.join(BASE_DIR,'data','csvdata', '')

#TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates', '') 


#TEMPLATE_FILE = TEMPLATE_DIR + 'REGISTER20190402.tabula-template.json'


#get folder files list

def get_files(month_path):



	fileslist = next(os.walk(month_path))[2]

	file_list_no_extension = []

	
	for filename in fileslist:
		new_name = filename.strip('.pdf')

		if compile_string.search(filename):

			file_list_no_extension.append(new_name)


	return file_list_no_extension


#convert file to csv



def convert_csv(filename):

	FILE_LOCATION_PDF =  PDF_DIR + filename + '.pdf'
	FILE_LOCATION_CSV = CSV_DIR + filename + '.csv'

	print(FILE_LOCATION_PDF)
	print(FILE_LOCATION_CSV)

	
	tables = camelot.read_pdf(FILE_LOCATION_PDF,  flavor='stream', pages='all')

	count = 1
	#tables = cm.read_pdf(file, pages='all')
	df = pd.concat([tab.df for tab in tables], ignore_index=True)
#	df.to_csv(file.split('.')[0]+'_camelot.csv')
	df.to_csv(FILE_LOCATION_CSV)

def convert_csv_path(file_name, month):

	# FILE_LOCATION_PDF =  PDF_DIR + filename + '.pdf'
	# FILE_LOCATION_CSV = CSV_DIR + filename + '.csv'

	FILE_LOCATION_PDF =  os.path.join(PDF_DIR, month, file_name) + '.pdf'
	FILE_LOCATION_CSV = os.path.join(CSV_DIR, month, file_name) +  '.csv'

	print(FILE_LOCATION_PDF)
	print(FILE_LOCATION_CSV)

	
	tables = camelot.read_pdf(FILE_LOCATION_PDF,  flavor='stream', pages='all')

	count = 1
	#tables = cm.read_pdf(file, pages='all')
	df = pd.concat([tab.df for tab in tables], ignore_index=True)
#	df.to_csv(file.split('.')[0]+'_camelot.csv')
	df.to_csv(FILE_LOCATION_CSV)


def main():
	global BASE_DIR
	skiped_names = []

	list_of_months = ['feb', 'mar']

	#list_of_months = ['jan','feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']

	

	for mon in list_of_months:

		path_to_month = os.path.join(BASE_DIR, 'pdfdata','data', mon )

		print(path_to_month)


		List_of_names = get_files(path_to_month)

		print (List_of_names)



		for file_n in List_of_names:


			info = 'working on ' + file_n
			print(info)


			try:

				convert_csv_path(file_n, mon)
				print ('worked on  ' + file_n)
			except Exception as e:
				skiped_names.append(file_n)
				#raise e


		print (skiped_names)






if __name__ == '__main__':
	main()