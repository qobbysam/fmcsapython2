import os

import clevercsv as csv
import itertools 
import re


out_csv_filename_name_change = "name_change_mar_7.csv"

out_csv_filename_grant_decision = "grants_mar_7.csv"

BASE_DIR = os.getcwd()

DATA_DIR = os.path.join(BASE_DIR, 'data', 'com')

DATA_DIR_TEST = os.path.join(BASE_DIR,'data', 'com_t')

gb_file_name = "escape_test_7.csv"

gb_file_name_not_eight =  "not_eight_7.csv"


'''
Need to fix the exceptions in not_eight garbage collector

suspect rewrite index in pattern 2 if row one is does not have a newline

'''

def garbage_collector_not_eight(list_in):

	garbage_file = os.path.join(BASE_DIR, 'data', 'gb', gb_file_name_not_eight )

	with open(garbage_file, 'a', encoding="utf8") as gb_file:

		csv_file = csv.writer(gb_file, delimiter=",")


		for line in list_in:

			csv_file.writerow(line)

		crap_not_eight = []


def garbage_collector(list_in):

	garbage_file = os.path.join(BASE_DIR, 'data', 'gb', gb_file_name )

	with open(garbage_file, 'a', encoding="utf8") as gb_file:

		csv_file = csv.writer(gb_file, delimiter=",")


		for line in list_in:

			csv_file.writerow(line)


		crap = []




def get_start_end_lines(filename):





	in_csv_file = os.path.join(DATA_DIR, filename) + '.csv'

	name_changes_pattern = re.compile(r'\d+NAME\sCHANGES')

	grant_authority_pattern = re.compile(r'\d+CERTIFICATES,\sPERMITS')

	certicate_registration_pattern = re.compile(r'\d+CERTIFICATES\sOF\sREGISTRATION')

	#Decisions_patern = re.compile(r'\d+DECISIONS\sAND\sNOTICES')
	withdrawal_pattern = re.compile(r'\d+WITHDRAWAL')

	dismissal_pattern = re.compile(r'\d+DISMISSALS')

	revocation_pattern = re.compile(r'\d+REVOCATION\sACTIONS')



	info_out = {
			'name_change_start_line': 0,
			'grant_start_line': 0,
			'certificate_registration_start_line':0,
			'withdrawal_start_line': 0,
			'dismissal_start_line': 0,
			'revocation_start_line' : 0,

	}


	with open(in_csv_file, 'r', encoding="utf8") as cs_in:

		csv_file = csv.reader(cs_in, delimiter= ",")

		for key, line in enumerate(csv_file, 1):
			joined_cells = str(''.join(line))

			if name_changes_pattern.match(joined_cells):

				info_out['name_change_start_line'] = key

				print("found namechange at  {}".format(key))

			elif grant_authority_pattern.match(joined_cells):
				info_out['grant_authority_start_line'] =key

				print("authority_FOOUN at {} ".format(key))



			elif certicate_registration_pattern.match(joined_cells):
				info_out['certificate_registration_start_line'] = key
				print("certicate_registration_pattern at {} ".format(key))

				
			elif withdrawal_pattern.match(joined_cells):
				info_out['withdrawal_start_line'] = key
				print("withdraw at {} ".format(key))


			elif dismissal_pattern.match(joined_cells):
				print("dismissal at {} ".format(key))

				info_out['dismissal_start_line'] = key 


			elif revocation_pattern.match(joined_cells):
				print("revocation at {} ".format(key))

				info_out['revocation_start_line'] = key

		return info_out

		

	


	




def save_name_changes(filename, name_change_start_line, end_loop_line):

	in_csv_file = os.path.join(DATA_DIR, filename) + '.csv'

	name_change_csv = os.path.join(BASE_DIR,'data','outcsv', out_csv_filename_name_change ) 


	loop_start = name_change_start_line
	end_loop = end_loop_line

	crap = []


	with open(in_csv_file, 'r', encoding="utf8") as cs_in, open(name_change_csv, 'a', encoding="utf8") as cs_out:

		csv_file = csv.reader(cs_in, delimiter=",")
		out_csv_file = csv.writer(cs_out, delimiter=",") 
		name_change_line_pattern = re.compile(r'\w+-\d+\n')

		try:
			content = itertools.islice(csv_file, loop_start, end_loop)

		except Exception as e:

			print("numbers failed")
			print( e )

		for key, row in enumerate(content):
			# #print (row[0])
			# #print (row[1])
			# c = row[1].split("\n")

			
			if name_change_line_pattern.match(row[1]):

				c = re.split('-|\n|,',row[1])
				
				#print (c)



				# authority_initials= c[0]
				# digits = c[1]
				# company_name = c[2] 
				# city = c[3]
				# state = c[-1]

				date_added = int(re.search(r'\d+', filename).group())
				add_date = date_added
				applied_date = row[2]

				to_line = c 

				to_line.append(add_date)
				to_line.append(applied_date)

				#line_to_add = authority_initials + digits + company_name

				#out_csv_file.writerow(authority_initials,digits,company_name,city,state,add_date,applied_date)

				out_csv_file.writerow(to_line)

				#print(to_line)

			else:
				line_to_crap = row

				line_to_crap.append("from file \n {} on row \n {} in save_name_changes" .format(filename, key ))
				crap.append(line_to_crap)


	garbage_collector(crap)




			# print (mc_info)
			# print (name_city_state_info)
			#print (row[2])
			#print (row[3])



def get_files(month_path):

	fileslist = next(os.walk(month_path))[2]

	file_list_no_extension = []
	dates = []

	
	for filename in fileslist:
		new_name = filename.strip('.csv')

		file_list_no_extension.append(new_name)

	return file_list_no_extension


def save_grant_decisions(filename, grant_start_line, end_loop_line):

	in_csv_file = os.path.join(DATA_DIR, filename) + '.csv'

	grant_decision_csv = os.path.join(BASE_DIR,'data','outcsv', out_csv_filename_grant_decision ) 


	loop_start = grant_start_line
	end_loop = end_loop_line

	crap = []

	crap_not_eight = []


	with open(in_csv_file, 'r', encoding="utf8") as cs_in, open(grant_decision_csv, 'a', encoding="utf8") as cs_out:

		csv_file = csv.reader(cs_in, delimiter=",")
		out_csv_file = csv.writer(cs_out, delimiter=",")
		grant_line_pattern_one = re.compile(r'\w+-\d+-\w\n')
		grant_line_pattern_two = re.compile(r'\w+-\d+\n')
		grant_line_pattern_three = re.compile(r'\w+-\d+-\w')

		try:
			content = itertools.islice(csv_file, loop_start, end_loop)

		except Exception as e:

			print("numbers failed")
			print( e ) 

		for key, row in enumerate(content):
			# #print (row[0])
			# #print (row[1])
			# c = row[1].split("\n")

			
			if grant_line_pattern_one.match(row[1]):

				to_line_prep = line_handler_common(row)



				# authority_initials= c[0]
				# digits = c[1]
				# company_name = c[2] 
				# city = c[3]
				# state = c[-1]

				date_added = int(re.search(r'\d+', filename).group())
				add_date = date_added
				

				to_line = to_line_prep 

				to_line.insert(-1, add_date)

				#line_to_add = authority_initials + digits + company_name

				#out_csv_file.writerow(authority_initials,digits,company_name,city,state,add_date,applied_date)
				if len(to_line) == 8:


					out_csv_file.writerow(to_line)

					print(to_line)

					to_line = []

				else:
					line_to_crap_not_eight = to_line

					line_to_crap_not_eight.append("from file \n {} on row \n {} in PATTERN_1" .format(filename, key ))
					crap_not_eight.append(line_to_crap_not_eight)

					line_to_crap_not_eight = []
					to_line = []






			elif grant_line_pattern_two.match(row[1]):

				res = re.search(r'\w+-\d+\n', row[1] )

				start_line, end_line = res.span()

				print(start_line, end_line)

				line_to_add_char = insert_broker_type(row[1], end_line-1) 

				to_line_prep = line_handler_common(line_to_add_char)

				date_added = int(re.search(r'\d+', filename).group())
				add_date = date_added
				

				to_line = to_line_prep 

				to_line.insert(-1, add_date)

				#line_to_add = authority_initials + digits + company_name

				#out_csv_file.writerow(authority_initials,digits,company_name,city,state,add_date,applied_date)

				if len(to_line) == 8:


					out_csv_file.writerow(to_line)

					print('used 2 %s ' % to_line)

				else:
					line_to_crap_not_eight = to_line

					line_to_crap_not_eight.append("from file \n {} on row \n {} in PATTERN_2" .format(filename, key ))
					crap_not_eight.append(line_to_crap_not_eight)

					line_to_crap_not_eight = []

					to_line = []


			elif grant_line_pattern_three.match(row[1]):


				try:
					to_line_prep = line_handler_other(row)
				except Exception as e:
					print ('line_to_prep in pattern 3 failed   %s' % e)

				

				date_added = int(re.search(r'\d+', filename).group())
				add_date = date_added
				

				to_line = to_line_prep 

				to_line.insert(-1, add_date)

				#line_to_add = authority_initials + digits + company_name

				#out_csv_file.writerow(authority_initials,digits,company_name,city,state,add_date,applied_date)

				if len(to_line) == 8:


					out_csv_file.writerow(to_line)

					print('used common %s ' % to_line)

				else:
					line_to_crap_not_eight = to_line

					line_to_crap_not_eight.append("from file \n {} on row \n {} in PATTERN_3" .format(filename, key ))
					crap_not_eight.append(line_to_crap_not_eight)

					line_to_crap_not_eight = []
					to_line =[]








			else:

				line_to_crap = row

				line_to_crap.append("from file \n {} on row \n {}" .format(filename, key ))
				crap.append(line_to_crap)

				line_to_crap = []


	garbage_collector(crap)

	garbage_collector_not_eight(crap_not_eight)


def line_handler_common(row):

	#split rows and for most common csv output



	c = re.split('-|\n|,',row[1])
	
	#print (c)


	applied_date = row[2]

	to_line = c 

	to_line.append(applied_date)

	return to_line


def line_handler_other(row):

	initials,number,auth_type = "failed_initials", "failed_number", "failed_auth_type"

	name,city, state = "failed_name", "failed_city", "failed_state"


	split_one = re.split('-|\n', row[1])

	split_two = re.split('-|,', row[2])


	if len(split_one) == 3 and len(split_two) == 3:



		try:
			initials, number, auth_type = split_one
		except Exception as e:
			print('initials number failed %s' % row[1])



		try:

			name, city, state = split_two
			
		except Exception as e:
			print('name,city, failed %s' % row[2] )

	


	if len(row[3]) != 0:

		add_date = row[3]

	else:
		add_date = row[4]

	to_line = []

	to_line.append(initials)
	to_line.append(number)
	to_line.append(auth_type)
	to_line.append(name)
	to_line.append(city)
	to_line.append(state)
	to_line.append(add_date)


	return to_line



	



def insert_broker_type(line_string, index):
	#print('line_string in insert_broker_type   %s at  %i' % line_string, str(index))
	line = line_string[:index] + '-C' + line_string[index:]

	print(line)
	return line


def main ():

	#get filelist



	filenames_list = get_files(DATA_DIR)


	for filename in filenames_list:

		# try:

		cur_file = get_start_end_lines(filename)
		name_change_start_line = cur_file['name_change_start_line']

		grant_start_line = cur_file['grant_start_line']
		certificate_registration_start_line = cur_file['certificate_registration_start_line']

		end_loop_line = cur_file['grant_authority_start_line'] - 1



		save_name_changes(filename, name_change_start_line, end_loop_line)


		end_loop_line = certificate_registration_start_line -1

		save_grant_decisions(filename, grant_start_line, end_loop_line)

		# except Exception as e:
		# 	print (e)
		# 	print("skipped you \n {}".format(filename) )

		



		# except Exception as e:
		# 	print ("could not save file \n {}" .format(filename))


