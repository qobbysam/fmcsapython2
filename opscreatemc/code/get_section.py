import os
import csv
import itertools 
import re




#imports
MONTH_C = 'apr'

BASE_DIR = os.getcwd()

CSV_OUT_DIR = os.path.join(BASE_DIR,'data','outcsv', MONTH_C , '')

#CSV_DIR = os.path.join(BASE_DIR,'csvdata', '')

CSV_DIR = os.path.join(BASE_DIR,'data','csvdata', MONTH_C , '')

CURRENT_CSV = 'REGISTER20200401'

IN_FILE_CSV = CSV_DIR + CURRENT_CSV +'.csv'

CHANGE_CSV_DIR = os.path.join(BASE_DIR,'data','outcsv','')
NAME_CHANGE_CSV = CHANGE_CSV_DIR + 'name_changes.csv'

GRANTS_CSV = CHANGE_CSV_DIR + 'grants_5.csv'

PAGE_COUNT = 0
line_match = ['2']

page_start_end_line = [('1','2','67')]

name_change_start_line = 0
grant_authority_start_line = 0
certificate_registration_start_line=0
withdrawal_start_line=0
dismissal_start_line =0
revocation_start_line =0

date_added = 'JAN,20,2020'




def get_start_end_lines():
	IN_FILE_CSV = CSV_DIR + CURRENT_CSV +'.csv'

	global name_change_start_line
	global grant_authority_start_line
	global certificate_registration_start_line
	global withdrawal_start_line
	global dismissal_start_line
	global revocation_start_line

	name_changes_pattern = re.compile(r'\d+NAME\sCHANGES')

	grant_authority_pattern = re.compile(r'\d+CERTIFICATES,\sPERMITS')

	certicate_registration_pattern = re.compile(r'\d+CERTIFICATES\sOF\sREGISTRATION')

	#Decisions_patern = re.compile(r'\d+DECISIONS\sAND\sNOTICES')
	withdrawal_pattern = re.compile(r'\d+WITHDRAWAL')

	dismissal_pattern = re.compile(r'\d+DISMISSALS')

	revocation_pattern = re.compile(r'\d+REVOCATION\sACTIONS')






	with open(IN_FILE_CSV, 'r') as cs_in:
		csv_file = csv.reader(cs_in, delimiter= ",")

		for key, line in enumerate(csv_file, 1):
			joined_cells = str(''.join(line))

			if name_changes_pattern.match(joined_cells):

				name_change_start_line = key

				print("found namechange at  {}".format(key))

			elif grant_authority_pattern.match(joined_cells):
				grant_authority_start_line =key

				print("authority_FOOUN at {} ".format(key))



			elif certicate_registration_pattern.match(joined_cells):
				certificate_registration_start_line = key
				print("certicate_registration_pattern at {} ".format(key))

				
			elif withdrawal_pattern.match(joined_cells):
				withdrawal_start_line = key
				print("withdraw at {} ".format(key))


			elif dismissal_pattern.match(joined_cells):
				print("dismissal at {} ".format(key))

				dismissal_start_line = key 


			elif revocation_pattern.match(joined_cells):
				print("revocation at {} ".format(key))

				revocation_start_line = key






def save_name_changes():
	global name_change_start_line
	global grant_authority_start_line
	global date_added

	loop_start = name_change_start_line
	end_loop = grant_authority_start_line-1


	with open(IN_FILE_CSV, 'r') as cs_in, open(NAME_CHANGE_CSV, 'a') as cs_out:

		csv_file = csv.reader(cs_in, delimiter=",")
		out_csv_file = csv.writer(cs_out, delimiter=",")
		name_change_line_pattern = re.compile(r'\w+-\d+\n')

		try:
			content = itertools.islice(csv_file, loop_start, end_loop)

			for row in content:
				# #print (row[0])
				# #print (row[1])
				# c = row[1].split("\n")

				
				if name_change_line_pattern.match(row[1]):
					c = re.split('-|\n|,',row[1])
					
					print (c)



					# authority_initials= c[0]
					# digits = c[1]
					# company_name = c[2]
					# city = c[3]
					# state = c[-1]
					add_date = date_added
					applied_date = row[2]

					to_line = c 

					to_line.append(add_date)
					to_line.append(applied_date)

					#line_to_add = authority_initials + digits + company_name

					#out_csv_file.writerow(authority_initials,digits,company_name,city,state,add_date,applied_date)

					out_csv_file.writerow(to_line)
					print(to_line)




				# print (mc_info)
				# print (name_city_state_info)
				#print (row[2])
				#print (row[3])
		except Exception as e:

			print("numbers failed")
			raise e



def save_grant_decision():
	#get_start_end_lines()
	global certificate_registration_start_line
	global grant_authority_start_line
	global date_added


	loop_start = grant_authority_start_line
	end_loop = certificate_registration_start_line - 1


	with open(IN_FILE_CSV, 'r') as cs_in, open(GRANTS_CSV, 'a') as cs_out:

		csv_file = csv.reader(cs_in, delimiter=",")
		out_csv_file = csv.writer(cs_out, delimiter=",")
		grant_line_pattern_one = re.compile(r'\w+-\d+-\w\n')

		grant_line_pattern_two = re.compile(r'')

		try:
			content = itertools.islice(csv_file, loop_start, end_loop)



		except Exception as e:
			print("file opening failed")
			print(e)


		
		for row in content:
			#print(row[1])

			line_to_add = ""

			if grant_line_pattern_one.match(row[1]):

				c = re.split('-|\n|,',row[1])

				line_to_add = c

				line_to_add.append(date_added)
				line_to_add.append(row[2])

				out_csv_file.writerow(line_to_add)

				#print(row[2])


				print (line_to_add)


# def save_revocations():
# 	global certificate_registration_start_line
# 	global grant_authority_start_line
# 	global date_added


# 	loop_start = grant_authority_start_line
# 	end_loop = certificate_registration_start_line - 1


# 	with open(IN_FILE_CSV, 'r') as cs_in, open(GRANTS_CSV, 'w') as cs_out:

# 		csv_file = csv.reader(cs_in, delimiter=",")
# 		out_csv_file = csv.writer(cs_out, delimiter=",")
# 		grant_line_pattern = re.compile(r'\w+-\d+-\w\n')

# 		try:
# 			content = itertools.islice(csv_file, loop_start, end_loop)

# 			for row in content:
# 				#print(row[1])


# 				if grant_line_pattern.match(row[1]):
# 					c = re.split('-|\n|,',row[1])

# 					line_to_add = c

# 					line_to_add.append(date_added)
# 					line_to_add.append(row[2])

# 					out_csv_file.writerow(line_to_add)

# 					#print(row[2])


# 					print (line_to_add)
# 		except Exception as e:
# 			raise e


def get_files(month_path):

	fileslist = next(os.walk(month_path))[2]

	file_list_no_extension = []
	dates = []

	
	for filename in fileslist:
		new_name = filename.strip('.csv')
		file_list_no_extension.append(new_name)
		
		


	# info = {
	# 	'fileNames': file_list_no_extension,
	# 	'date': dates
	# }


	return file_list_no_extension



def do_main_things(FILE_IN_NAME):

	global CURRENT_CSV 
	global date_added

	CURRENT_CSV = FILE_IN_NAME
	date = int(re.search(r'\d+', CURRENT_CSV).group())
	date_added = date


	get_start_end_lines()

	save_name_changes()

	save_grant_decision()





def main():
	global CSV_DIR
	global CURRENT_CSV

	

	list_of_months = ['jan','feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']

	for month in list_of_months:

		CSV_DIR = os.path.join(BASE_DIR, 'data', 'csvdata', month, '')

		m = get_files(CSV_DIR)



	#file_names = m['fileNames']
	#dates = m['date']

		for file_name in m:

			try:
				do_main_things(file_name)
				
			except Exception as e:

			#raise e

				print(file_name)
			#do_main_things(file_name)


		#print(dates)


if __name__ == '__main__':
	main()