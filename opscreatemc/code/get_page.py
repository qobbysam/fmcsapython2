import os
import csv
import itertools 
import re




#imports
MONTH_C = 'apr'

BASE_DIR = os.getcwd()

CSV_OUT_DIR = os.path.join(BASE_DIR,'outcsv', MONTH_C , '')

#CSV_DIR = os.path.join(BASE_DIR,'csvdata', '')

CSV_DIR = os.path.join(BASE_DIR,'csvdata', MONTH_C , '')

IN_FILE_CSV = CSV_DIR + 'REGISTER20190412.csv'

PAGE_COUNT = 0
line_match = ['2']

page_start_end_line = [('1','2','67')]


def check_page_end_pattern(check_str):
	end_page_pattern = re.compile(r'\d+Run\sT')

	if end_page_pattern.match(check_str):
		return True
	else:
		return False



def do_note(cur_line):
	global PAGE_COUNT
	global line_match
	page_number = str(PAGE_COUNT)
	start_line = int(line_match[-1]) + 1
	end_line = cur_line

	page_info = (page_number, start_line, end_line)

	line_match.append(end_line)

	page_start_end_line.append(page_info)


	#print ('adding :   {}' .format(page_info))


def check_page_end(csv_row_str, cur_line):

	global PAGE_COUNT
	global line_match

	if check_page_end_pattern(csv_row_str):
		
		PAGE_COUNT += 1 

		print ('added line')

		do_note(cur_line) 






def main():
	global PAGE_COUNT
	global line_match
	global page_start_end_line



	with open(IN_FILE_CSV, 'r') as m:
		reader = csv.reader(m, delimiter=',')

		for line, cont in enumerate(reader):

			line_str = str(''.join(cont))

			print (line_str)

			key = str(line) 

			check_page_end(line_str, key)

			# if check_page_end_pattern(o):


			# 	print("line : {} , c  :{} ".format(p,o))


		
	with open(IN_FILE_CSV, 'r') as nm:

		z = page_start_end_line

		p,s,e = z[3]

		print ('{}  {} {}'.format(p,s,e ))
		sn = int(s)
		en = int(e)

		print ('{}  {}'.format(sn,en ))

		print(type(sn))

		reads = csv.reader(nm, delimiter=',')

		its = itertools.islice(reads, sn, en)

		for ro in its:
			print (ro)		

	print (line_match)




if __name__ == '__main__':
	main()