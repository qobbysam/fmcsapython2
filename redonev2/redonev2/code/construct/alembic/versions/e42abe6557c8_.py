"""empty message

Revision ID: e42abe6557c8
Revises: 
Create Date: 2021-05-16 21:38:27.358729

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e42abe6557c8'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('construct_infos',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('update_count', sa.Integer(), nullable=True),
    sa.Column('check_count', sa.Integer(), nullable=True),
    sa.Column('needs_update', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('mc_objects',
    sa.Column('mc_id', sa.String(), nullable=False),
    sa.Column('mc_number', sa.String(), nullable=True),
    sa.Column('mc_type', sa.String(), nullable=True),
    sa.Column('mc_in', sa.String(), nullable=True),
    sa.Column('mc_company_name', sa.String(), nullable=True),
    sa.Column('mc_city', sa.String(), nullable=True),
    sa.Column('mc_state', sa.String(), nullable=True),
    sa.Column('mc_grant_date', sa.Date(), nullable=True),
    sa.Column('mc_apply_date', sa.Date(), nullable=True),
    sa.PrimaryKeyConstraint('mc_id')
    )
    op.create_table('fmcsa_objects',
    sa.Column('construct_info_id', sa.Integer(), nullable=True),
    sa.Column('dot_number', sa.Integer(), nullable=False),
    sa.Column('mc_number', sa.String(), nullable=True),
    sa.Column('legal_name', sa.String(), nullable=True),
    sa.Column('dba_name', sa.String(), nullable=True),
    sa.Column('carrier_operation', sa.String(), nullable=True),
    sa.Column('dot_add_date', sa.Date(), nullable=True),
    sa.Column('mc_add_date', sa.Date(), nullable=True),
    sa.Column('oic_state', sa.String(), nullable=True),
    sa.Column('nbr_power_units', sa.Integer(), nullable=True),
    sa.Column('driver_total', sa.Integer(), nullable=True),
    sa.Column('phy_street', sa.String(), nullable=True),
    sa.Column('phy_city', sa.String(), nullable=True),
    sa.Column('phy_state', sa.String(), nullable=True),
    sa.Column('phy_zip', sa.String(), nullable=True),
    sa.Column('phy_country', sa.String(), nullable=True),
    sa.Column('mailing_street', sa.String(), nullable=True),
    sa.Column('mailing_city', sa.String(), nullable=True),
    sa.Column('mailing_state', sa.String(), nullable=True),
    sa.Column('mailing_zip', sa.String(), nullable=True),
    sa.Column('mailing_country', sa.String(), nullable=True),
    sa.Column('mcs150_date', sa.Date(), nullable=True),
    sa.Column('mcs150_mileage', sa.String(), nullable=True),
    sa.Column('mcs150_mileage_year', sa.String(), nullable=True),
    sa.Column('telephone', sa.String(), nullable=True),
    sa.Column('fax', sa.String(), nullable=True),
    sa.Column('email_address', sa.String(), nullable=True),
    sa.ForeignKeyConstraint(['construct_info_id'], ['construct_infos.id'], ),
    sa.PrimaryKeyConstraint('dot_number')
    )
    op.create_table('fmcsa_object_extras',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('a_fmcsa_object_id', sa.Integer(), nullable=True),
    sa.Column('hm_flag', sa.String(), nullable=True),
    sa.Column('pc_flag', sa.String(), nullable=True),
    sa.ForeignKeyConstraint(['a_fmcsa_object_id'], ['fmcsa_objects.dot_number'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('fmcsa_object_extras')
    op.drop_table('fmcsa_objects')
    op.drop_table('mc_objects')
    op.drop_table('construct_infos')
    # ### end Alembic commands ###
