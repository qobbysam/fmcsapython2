import os
import sys
import time
import csv
import pickle

import uuid

from collections import deque
sys.path.append("..")



from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker



from params import params

from helpers.cleanops import CleanDirty, CleanDirtyNostrip


from tables_red import Base

from tables_red import Construct_Info, Fmcsa_object, Fmcsa_object_extra , Mc_object



"""	Need to fix garbage collector for this.
	improve the data reported






 """
BASE_DIR = os.getcwd()

DATA_DIR = os.path.join(BASE_DIR,'data', '')

#CSV_FILE = DATA_DIR + 'sample.csv'
CSV_FILE = DATA_DIR + 'grants_mar_7.csv'




START_TIME = time.time()

END_TIME = time.time()


CURRENT_LINE = 0


params = params()

db_url = 'postgresql://' + params['user']+ ':'+params['password'] + '@' + params['host'] + '/' + params['database']

engine = create_engine(db_url, echo = True)

Session = sessionmaker(bind=engine)


Purge = CleanDirty()
purge_string = Purge.clean_string
purge_number = Purge.clean_number
purge_date = Purge.clean_date




updated_count = 0

objects_to_db = []#deque([])

finder_updater_list = []#deque([])

finder_updater_list_not_looped = []#deque([])


updater_list_to_db = []#deque([])

skipped_mc_file_name = 'missing.txt'
#not_found_mc_list = deque([])




def do_save_objects():

	#data_to_db = load_all_obj(PICKLE_DB)
	global objects_to_db
	global CURRENT_LINE
	global START_TIME

	time_now = time.time()


	mgs_p = '''
				xxxxxxx{}xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
				xxxxxxxxxxxx{}xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
				xxxxxxxxxxxxxxxxxxxxxxxxx{}xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
			''' 

	print(mgs_p.format(START_TIME, CURRENT_LINE, time_now))

	#print(objects_to_db)

	ses = Session()

	ses.add_all(objects_to_db)


	ses.commit()

	objects_to_db = []#deque([])

	finder_updater()





def finder_updater():


	print (""" 
			qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
			qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
			pppppppppppppppppppppppppppppppppppppp
			pppppppppppppppppppppppppppppppppppppp
		""")

	s = Session()


	global finder_updater_list

	global updater_list_to_db

	global updated_count


	#get all data to varible


	#list_of_db =  s.query(Fmcsa_object).yeild_per(60000)




	for i in s.query(Fmcsa_object).filter_by(carrier_operation = 'A').yield_per(100000):

		legal_name = getattr(i, 'legal_name')
		phy_city = getattr(i, 'phy_city')
		phy_state = getattr(i, 'phy_state')

		





		#print (legal_name) 

		for ob in finder_updater_list:

			finder = ob[0]

			updater = ob[1]

			#print (updater['mc_number'])


			if legal_name == finder['legal_name'] and phy_city == finder['phy_city'] and  phy_state == finder['phy_state']:

				preped_class = i 

				setattr(preped_class,'mc_number' ,updater['mc_number'])
				setattr(preped_class, 'mc_add_date', updater['mc_add_date'])

				print('appending a class here ready ')

				print(updater)

				updater_list_to_db.append(preped_class)

				finder_updater_list.remove(ob)


			


	skipped_you()
	#finder_updater_list = deque([])

	print( ''' 
			p999999999999999999999922222222222222222211111111111111
			1111111111111111111111111111111113333333333333333333333
			ccccccccccccccccccccccccccccccccccccccccccccccc
			dddddddddddddd
			{}
		'''.format(updated_count))



	s.add_all(updater_list_to_db)

	s.commit()

	updated_count = updated_count + len(updater_list_to_db)
	updater_list_to_db= [] #deque([])

	#do_update()



# def do_update():
# 	global updater_list_to_db


# 	print( ''' 
# 			p999999999999999999999922222222222222222211111111111111
# 			1111111111111111111111111111111113333333333333333333333
# 		''')

# 	s = Session()


# 	s.add_all(updater_list_to_db)

# 	s.commit()

# 	updater_list_to_db= deque([])




	# for ob in finder_updater_list:

	# 	finder = ob[0]

	# 	updater = ob[1]
 
	# 	for i in list_of_db:

	# 		checker = getattr(i, **finder)










	

	# for ob in finder_updater_list:

	# 	finder = ob[0]

	# 	updater = ob[1]

	# 	try:
	# 		m = s.query(Fmcsa_object).filter_by(**finder).first()


	# 		m.update(**updater)
	# 		print (""" 
	# 				qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
	# 				qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
	# 			""")
	# 		s.flush()
	# 	except Exception as e:

	# 		print (""" 
					
	# 				pppppppppppppppppppppppppppppppppppppp
	# 				pppppppppppppppppppppppppppppppppppppp
	# 			""")
	# 		print (e)

	# s.commit()


	#finder_updater_list = []

		








def timeops():
	global START_TIME
	global END_TIME
	print ('ST time {} and end time {}'.format(time.ctime(START_TIME), time.ctime(END_TIME)))
	print('updated')
	print (updated_count)

def check_if_exists(dot_number):

	check_session = Session()

	check_count = check_session.query(Fmcsa_object).get(dot_number)

	if check_count is not None:
		return True
	else:
		return False




#['MC', '1025780', 'MIDNIGHT TOWING RECOVERY INC ', ' OAK LAWN', ' IL', 20201230, '03/27/2020']

#['MC', '1092912', 'C', 'KNIGHT & SON CORP ', ' SOMERSET', ' MA', '20200429', '02/21/2020']

def do_f(args):

	global finder_updater_list_not_looped

	global finder_updater_list


	data_in = args

	#print (data_in)

	empty_string = ""
	empty_date = '2001,JAN,01'

	uu_id = uuid.uuid4().hex

	#print (uu_id)

	# mc_object_dict = {


	# 	'mc_id' : uu_id,

	# 	'mc_number' : purge_number(data_in[1]),
		
	# 	'mc_in' : purge_string(data_in[0]),
	#     'mc_company_name'  : purge_string(data_in[3]) ,
	#     'mc_type': purge_string([2]),
	#     'mc_city' : purge_string(data_in[4]),
	#     'mc_state':purge_string(data_in[5]),
	#     'mc_grant_date': purge_date(data_in[6]),

	#     'mc_apply_date': purge_date(data_in[7]) ,
	# }



	mc_object_dict = {


	'mc_id' : uu_id,


	'mc_number' : purge_number(data_in[1]),
		
	'mc_in' : purge_string(data_in[0]),

	'mc_company_name'  : purge_string(data_in[3]) ,

	'mc_type': purge_string(data_in[2]),

	'mc_city' : purge_string(data_in[4]),

	'mc_state':purge_string(data_in[5]),

	'mc_grant_date': purge_date(data_in[6]),

	'mc_apply_date': purge_date(data_in[7]) ,



	}


	finder_dict = {

		'legal_name': purge_string(data_in[3]),
		'phy_state': purge_string(data_in[5]) ,
		'phy_city' : purge_string(data_in[4])
	}

	update_dict = {
		'mc_number' : purge_string(data_in[1]),
		'mc_add_date': purge_date(data_in[6])
	}
	

	#print (finder_dict)

	mc_object_ins = Mc_object(**mc_object_dict)

	#print (mc_object_dict)

	objects_to_db.append(mc_object_ins)

	
	finder_updater_list.append([finder_dict,update_dict])

	finder_updater_list_not_looped.append([finder_dict, update_dict])




	if len(objects_to_db) % 10000 == 0:

		do_save_objects()
	



def diff_list(list1, list2):

	name_mc_not_looped = []

	name_mc_looped = []

	


	for item in  list2:

		line_to_append = item[1]['mc_number'] + '****'+item[0]['legal_name']
		name_mc_looped.append(line_to_append)

	for item in  list1:

		line_to_append_ = item[1]['mc_number'] + '****' + item[0]['legal_name']
		name_mc_not_looped.append(line_to_append_)




	# set_1 = set(map(tuple, list1))
	# set_2 = set(map(tuple, list2))

	return list(set(name_mc_not_looped).symmetric_difference(set(name_mc_looped)))


def skipped_you():

	global finder_updater_list_not_looped
	global finder_updater_list

	global skipped_mc_file_name




	skipped_mc_path = os.path.join(DATA_DIR, 'mc_ops', skipped_mc_file_name )
	#[item for item in temp1 if item not in temp2]
	msg = diff_list(finder_updater_list_not_looped, finder_updater_list)


	with open (skipped_mc_path, 'a+') as skiped:

		#skiped_file = csv.writer(skiped, delimiter=',')

		for item in msg:
			skiped.write("%s\n" % item)



	print('skipped you     {}'. format(len(msg)))


	finder_updater_list = []#deque([])
	finder_updater_list_not_looped = []#deque([])



	


def main():

	#pass

	global END_TIME
	global objects_to_db
	global CURRENT_LINE

	print("main is called")

	try:
		

		with open(CSV_FILE,'r', encoding="utf8") as fo:

			fmcsa = csv.reader(fo, delimiter="," )


			for key, line in enumerate(fmcsa):
				CURRENT_LINE = key

				#print(key)

				#print(line)

			
				#print('wwwwwwwwwsssssssssscccccccccccccddddddddddds')

				luke = line

				#print(type(luke))

				#print (line)


				if len(luke) == 0:

					pass

					##print ('skiping you')

				else:

					try:
						do_f(luke)

					except Exception as e:

						print('could not do_F')

						print (e)

						print (luke)




					#print(m)


					#dot_check = m[0]

					#check_first = check_if_exists(dot_check)

					# if check_first:
					# 	skipped_you(m)
						

					# else:
					# # 	do_f(m)
						

			END_TIME = time.time()

			timeops()

			#print(objects_to_db)

			do_save_objects()
		END_TIME = time.time()
		timeops()



	except Exception as e:
		END_TIME = time.time()
		timeops()

		print (e)



# if __name__ == '__main__':
# 	main()