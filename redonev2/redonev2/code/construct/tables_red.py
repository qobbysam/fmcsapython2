import os
import sys
import time

sys.path.append("..")



from construct.params import params

#sys.path.append("./construct")

from sqlalchemy import Column, Integer, String, Boolean, Date, ForeignKey, Sequence
from sqlalchemy.ext.declarative import declarative_base 
from sqlalchemy.orm import relationship
from sqlalchemy.orm.attributes import InstrumentedAttribute




from random import randint 

import uuid




# params = params()

# db_url = 'postgresql://' + params['user']+ ':'+params['password'] + '@' + params['host'] + '/' + params['database']

# engine = create_engine(db_url, echo = True)

# Session = sessionmaker(bind=engine)


def random_integer():
    min_ = 1000
    max_ = 1000000000

    _min = 2000
    _max = randint(30000, 10000000)
    rand = randint(min_, max_) + randint(_min, _max)

    # possibility of same random number is very low.
    # but if you want to make sure, here you can check id exists in database.
    
    # db_session_maker = sessionmaker(bind=engine)
    # db_session = Session()
    
    # while db_session.query(Construct_Info).filter(id == rand).limit(1).first() is not None:
    # 	rand = randint(min_, max_)

    return rand

# def random_integer_extra():
#     min_ = 100
#     max_ = 1000000000
#     rand = randint(min_, max_)

#     # possibility of same random number is very low.
#     # but if you want to make sure, here you can check id exists in database.
    
#     # db_session_maker = sessionmaker(bind=engine)
#     db_session = Session()
    
#     while db_session.query(Fmcsa_object_extra).filter(id == rand).limit(1).first() is not None:
#     	rand = randint(min_, max_)

#     return rand

# from params import params


# #postgresql+psycopg2://scott:tiger@localhost/mydatabase

# #dialect+driver://username:password@host:port/database

# #db_url =r' postgresql+psycopg2://{username}:{password}@{host}:{port}/{dbname}'

# params = params()

# db_url = 'postgresql://' + params['user']+ ':'+params['password'] + '@' + params['host'] + '/' + params['database']

# engine = create_engine(db_url, echo = True)

# base = declarative_base() 

# class Film(base):
#     __tablename__ = 'films'

# 	title = Column(String, primary_key=True)
# 	director = Column(String)
# 	year = Column(String)


Base = declarative_base()


class Construct_Info(Base):
	__tablename__ = 'construct_infos'

	id = Column(Integer, primary_key=True, default=random_integer)
	update_count = Column(Integer)
	check_count  = Column(Integer)
	needs_update = Column(Boolean)

	an_fmcsa_object = relationship("Fmcsa_object")

	#an_fmcsa_object = relationship("Fmcsa_object", uselist= False, back_populates='construct_info')

	def __repr__(self):

		return "<Construct_info(id='%s')>" % (self.id)




class Fmcsa_object(Base):
	__tablename__ = 'fmcsa_objects'
 
	
	

	

	#a_construct_info    = relationship("Construct_Info")
	#a_construct_info    = relationship("Construct_Info", back_populates="an_fmcsa_object")

	
	#an_fmcsa_object_extra = relationship("Fmcsa_object_extra", uselist=False, back_populates="fmcsa_object")

	construct_info_id = Column(Integer, ForeignKey('construct_infos.id'))
	
	
	dot_number = Column(Integer, primary_key=True)

	mc_number  = Column(String)
	legal_name = Column(String)
	dba_name   = Column(String)

	carrier_operation = Column(String)

	dot_add_date =Column(Date)
	mc_add_date  = Column(Date)

	oic_state = Column(String)
	nbr_power_units = Column(Integer)
	driver_total = Column(Integer)

	phy_street  = Column(String)
	phy_city    = Column(String)
	phy_state   = Column(String)
	phy_zip     = Column(String)
	phy_country = Column(String)

	
	mailing_street =Column(String)
	mailing_city   = Column(String)
	mailing_state  = Column(String)
	mailing_zip     = Column(String)
	mailing_country  = Column(String)

	mcs150_date = Column(Date)
	mcs150_mileage = Column(String)
	mcs150_mileage_year = Column(String)
	
	telephone = Column(String)
	fax = Column(String)
	email_address = Column(String)


	an_fmcsa_object_extra = relationship("Fmcsa_object_extra")


	def __repr__(self):
		return "<Fmcsa_object(dot_number='%s')>" % (self.dot_number)


	def update(self, **kwargs):

		for key, value in kwargs.items():
			if hasattr(self,key):
				setattr(self, key, value)

	def __iter__(self):
		return self.__dict__.iteritems()





class Fmcsa_object_extra(Base):

	__tablename__ = 'fmcsa_object_extras'




	#_fmcsa_object = relationship("Fmcsa_object", back_populates="an_fmcsa_object_extra")

	#_fmcsa_object = relationship("Fmcsa_object")

	id = Column(Integer,primary_key=True )



	a_fmcsa_object_id = Column(Integer, ForeignKey('fmcsa_objects.dot_number'))

	hm_flag = Column(String)
	pc_flag = Column(String)




	def __repr__(self):
		return "<Fmcsa_object_extra(id='%s')>" % (self.id)



class Mc_object(Base):

	__tablename__ = 'mc_objects'

	mc_id = Column(String, primary_key=True)

	mc_number = Column(String)

	mc_type = Column(String)

	mc_in = Column(String)

	mc_company_name = Column(String)

	mc_city = Column(String)

	mc_state = Column(String)

	mc_grant_date = Column(Date)

	mc_apply_date = Column(Date)


	def __repr__(self):

		return "<Mc_object(id='%s')>" % (self.id)


