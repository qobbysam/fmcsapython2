#!/usr/bin/python

import sys
sys.path.append("..")
from configparser import ConfigParser

def params(filename='database.ini', section='postgresql'):

	parser = ConfigParser()
	parser.read(filename)

	db= {}
	if parser.has_section(section):
		param = parser.items(section)
		for para in param:
			db[para[0]] = para[1]
	else: raise Exception('section {0} not found in the {1} file'.format(section, filename))

	return db

	