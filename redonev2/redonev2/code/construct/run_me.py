import os
import sys
import time
import csv
import pickle

from collections import deque
sys.path.append("..")



from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker



from params import params

from helpers.cleanops import CleanDirty


from tables_red import Base

from tables_red import Construct_Info, Fmcsa_object, Fmcsa_object_extra


BASE_DIR = os.getcwd()

DATA_DIR = os.path.join(BASE_DIR,'data', '')

#CSV_FILE = DATA_DIR + 'sample.csv'
CSV_FILE = DATA_DIR + 'FMCSA_CENSUS1_2021Apr.txt'

PICKLE_DB = DATA_DIR + 'ex.pickle'


START_TIME = time.time()

END_TIME = time.time()


CURRENT_LINE = 0
# #postgresql+psycopg2://scott:tiger@localhost/mydatabase

# #dialect+driver://username:password@host:port/database

# #db_url =r' postgresql+psycopg2://{username}:{password}@{host}:{port}/{dbname}'


# data_in = [

# 1003780,
# 'DENNIS GAY FREMSTAD',
# 'DENNIS FREMSTAD TRUCKING',
# 'C',
# 'N',
# 'N',
# 'N39995 LAW LANE',
# 'WHITEHALL',
# 'WI',
# '54773',
# 'US',
# 'N39995 LAW LANE',
# 'WHITEHALL',
# 'WI',
# '54773',
# 'US',
# '(715) 299-5709',
# '',
# 'KELLY@TRUCKINGPERMITSERVICE.COM',
# '16-NOV-20',
# 65000,
# '2019',
# '08-FEB-02',
# 'WI',
# 1,
# 1
# ]

params = params()

db_url = 'postgresql://' + params['user']+ ':'+params['password'] + '@' + params['host'] + '/' + params['database']

engine = create_engine(db_url, echo = True)

Session = sessionmaker(bind=engine)


Purge = CleanDirty()
purge_string = Purge.clean_string
purge_number = Purge.clean_number
purge_date = Purge.clean_date
#check database




# construct_info_dict = {
	
# 	  'update_count' : 1,
#     'check_count'  : 1 ,
#     'needs_update' : False,
# }


# fmcsa_object_dict = {

#     'construct_info_id': ,
# 	'construct_info':,
# 	'an_fmcsa_object_extra':,
# 	'dot_number':,
# 	'mc_number': ,
# 	'legal_name': ,
# 	'dba_name': ,
# 	'carrier_operation': ,
# 	'dot_add_date':,
# 	'mc_add_date': ,
# 	'oic_state': ,
# 	'nbr_power_units': ,
# 	'driver_total': , 
# 	'phy_street': ,
# 	'phy_city': ,   
# 	'phy_state': ,  
# 	'phy_zip': ,    
# 	'phy_country': ,
# 	'telephone': ,
# 	'fax': ,
# 	'email': 
# }


# fmcsa_object_extra_dict = {
	
# 	'a_fmcsa_object_id': ,
# 	'fmcsa_object': ,
# 	'hm_flag': ,
# 	'pc_flag': ,

# 	'mailing_street': ,
# 	'mailing_city': ,
# 	'mailing_state': ,
# 	'mailing_zip': ,
# 	'mailing_country': ,
# 	'mcs150_date': ,
# 	'mcs150_mileage': ,
# 	'mcs150_mileage_year' 
# }


#create database

# def create_construct_info(**kwargs):

# 	session_construct = Session()

# 	data_in = **kwargs

# 	new_construct_info_obj = Construct_Info(data_in)

# 	session_construct.add(new_construct_info_obj)
# 	session.commit()




# def create_fmcsa_object(**kwargs):


# 	pass




# def create_fmcsa_object_extra():
# 	pass
# #loop files for database

objects_to_db = deque([])



# def append_pick(ob):
# 	with open(PICKLE_DB ,'wb') as handle:
# 		pickle.dump(ob, handle)


# def load_all_obj(lin):
# 	with open(lin, 'rb') as handle:
# 		while True:
# 			try:
# 				yield pickle.load(handle)
# 			except:
# 				break

def do_save_objects():

	#data_to_db = load_all_obj(PICKLE_DB)
	global objects_to_db
	global CURRENT_LINE
	global START_TIME

	time_now = time.time()


	mgs_p = '''
				xxxxxxx{}xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
				xxxxxxxxxxxx{}xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
				xxxxxxxxxxxxxxxxxxxxxxxxx{}xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
			''' 

	print(mgs_p.format(START_TIME, CURRENT_LINE, time_now))


	#print(objects_to_db)

	ses = Session()
	ses.add_all(objects_to_db)

	ses.commit()

	objects_to_db = []




def timeops():
	global START_TIME
	global END_TIME
	print ('ST time {} and end time {}'.format(time.ctime(START_TIME), time.ctime(END_TIME)))

def check_if_exists(dot_number):

	check_session = Session()

	check_count = check_session.query(Fmcsa_object).get(dot_number)

	if check_count is not None:
		return True
	else:
		return False







def do_f(args):

	data_in = args

	#print (data_in)

	empty_string = ""
	empty_date = '2001,JAN,01'

	construct_info_dict = {

		'id' : purge_number(data_in[0]),
		
		'update_count' : 1,
	    'check_count'  : 1 ,
	    'needs_update' : False,
	}


	fmcsa_object_dict = {

	    'construct_info_id': purge_number(data_in[0]) ,
		

		'dot_number': purge_number(data_in[0]),

		'mc_number': empty_string ,

		'legal_name': purge_string(data_in[1]),

		'dba_name': purge_string(data_in[2]) ,

		'carrier_operation': purge_string(data_in[3]) ,

		'dot_add_date': purge_date(data_in[22]),

		'mc_add_date': empty_date,

		'oic_state': purge_string(data_in[23]),

		'nbr_power_units': purge_number(data_in[24]) ,

		'driver_total': purge_number(data_in[25]) , 

		'phy_street':purge_string(data_in[6]) ,

		'phy_city':purge_string(data_in[7]) ,  

		'phy_state':purge_string(data_in[8]) , 

		'phy_zip': purge_string(data_in[9]),  

		'phy_country': purge_string(data_in[10]) ,

		'mailing_street':purge_string(data_in[11]) ,

		'mailing_city':purge_string(data_in[12]) ,

		'mailing_state': purge_string(data_in[13]),

		'mailing_zip':purge_string(data_in[14]) ,

		'mailing_country': purge_string(data_in[15]),

		'mcs150_date': purge_date(data_in[19]) ,

		'mcs150_mileage': purge_string(data_in[20]) ,

		'mcs150_mileage_year' : purge_string(data_in[21]),

		'telephone': purge_string(data_in[16]) ,

		'fax': purge_string(data_in[17]) ,

		'email_address': purge_string(data_in[18])
	}


	fmcsa_object_extra_dict = {

		'id' : purge_number(data_in[0]),
		
		'a_fmcsa_object_id': purge_number(data_in[0]),
		
		'hm_flag': purge_string(data_in[4]) ,

		'pc_flag': purge_string(data_in[5]),


	}

	#session_main = Session()


	#print (construct_info_dict)

	#cid = SimpleNamespace(**construct_info_dict)



	construct_info_ins = Construct_Info(**construct_info_dict )

	#append_pick(construct_info_ins)

	objects_to_db.append(construct_info_ins)

	#session_main.add(construct_info_ins)
	#session_main.commit()


	#fmcsa_object_dict['construct_info_id'] =construct_info_ins.id


	fmcsa_object_ins = Fmcsa_object(**fmcsa_object_dict)
	#append_pick(fmcsa_object_ins)

	objects_to_db.append(fmcsa_object_ins)


	#session_main.add(fmcsa_object_ins)
	#session_main.commit()

	#fmcsa_object_extra_dict['a_fmcsa_object_id'] = fmcsa_object_ins.dot_number

	fmcsa_object_extra_ins = Fmcsa_object_extra(**fmcsa_object_extra_dict)
	#append_pick(fmcsa_object_extra_ins)

	objects_to_db.append(fmcsa_object_extra_ins)


	if len(objects_to_db) % 100000 == 0:

		do_save_objects()
	
	#print (fmcsa_object_extra_ins)

	# session_main.add(fmcsa_object_extra_ins)

	# session_main.commit()



	#print("added all of this successfully {}".format(args))



def skipped_you(args):
	msg = args
	print('skipped you     {}'. format(msg))


def main():

	global END_TIME
	global objects_to_db
	global CURRENT_LINE

	print("main is called")

	try:
		

		with open(CSV_FILE, 'r', encoding='utf-8', errors='ignore') as fo:
			fmcsa = csv.reader(fo, delimiter="," )
			for key, line in enumerate(fmcsa):

				#line = line.decode(line).encode('utf-8')

				CURRENT_LINE = key
				if key == 0  :

					print('wwwwwwwwwsssssssssscccccccccccccddddddddddds')



				elif len(line) < 26:

					print("skipping you \n {}".format(line))





				else:

					m = line


					do_f(m)


					#print(m)


					#dot_check = m[0]

					#check_first = check_if_exists(dot_check)

					# if check_first:
					# 	skipped_you(m)
						

					# else:
					# # 	do_f(m)
						

			END_TIME = time.time()

			timeops()

			#print(objects_to_db)

			do_save_objects()
		END_TIME = time.time()
		timeops()



	except Exception as e:
		END_TIME = time.time()
		timeops()

		print(e)



# if __name__ == '__main__':
# 	main()