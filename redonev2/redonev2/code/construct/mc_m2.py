import os
import sys
import time
import csv
import pickle
import string
import uuid


from multiprocessing import Pool
from collections import deque
sys.path.append("..")

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from sqlalchemy.orm import load_only
from params import params
from helpers.cleanops import CleanDirty, CleanDirtyNostrip


from tables_red import Base

from tables_red import Construct_Info, Fmcsa_object, Fmcsa_object_extra , Mc_object


BASE_DIR = os.getcwd()

DATA_DIR = os.path.join(BASE_DIR,'data', '')

#CSV_FILE = DATA_DIR + 'sample.csv'
CSV_FILE = DATA_DIR + 'new_grants_1.csv'




START_TIME = time.time()

END_TIME = time.time()


CURRENT_LINE = 0


params = params()

db_url = 'postgresql://' + params['user']+ ':'+params['password'] + '@' + params['host'] + '/' + params['database']

engine = create_engine(db_url, echo = True)

Session = sessionmaker(bind=engine)

cur_line = ""


Purge = CleanDirty()
purge_string = Purge.clean_string
purge_number = Purge.clean_number
purge_date = Purge.clean_date




updated_count = 0

objects_to_db = []#deque([])

finder_updater_list = []#deque([])

finder_updater_list_not_looped = []#deque([])


updater_list_to_db = []#deque([])

skipped_mc_file_name = 'new_miss.txt'
#not_found_mc_list = deque([])





values = 'dot_number', 'legal_name', 'dba_name', 'phy_city','phy_state','mailing_city', 'mailing_state', 'carrier_operation'


def pool_filter(pool, func, candidates):
    return [c for c, keep in zip(candidates, pool.map(func, candidates)) if keep]


def p():

    s = Session()

    #query_statement = s.query(Fmcsa_object).with_entities(Fmcsa_object.dot_number, )
    #updater_list = s.query(Fmcsa_object).filter_by(carrier_operation = 'A').values(values).all()
    updater_list =  [ u.__dict__ for u in  s.query(Fmcsa_object).options(load_only(*values)).filter_by(carrier_operation = 'A').all()]
    sorted_list = sorted(updater_list, key= lambda i:i['legal_name'])


    return sorted_list 













def timeops():
	global START_TIME
	global END_TIME
	print ('ST time {} and end time {}'.format(time.ctime(START_TIME), time.ctime(END_TIME)))
	print('updated')
	print (updated_count)





def clean_text(s):

    legal_name = s['legal_name']

    #print (legal_name)
    
    new_legal_name = purge_string_puncs(legal_name)
    s['legal_name'] = new_legal_name

    return s 



def purge_string_puncs(text_in):



    return ''.join(e for e in text_in if e.isalnum())



def produce_map(dict_mc, dict_fmcsa):

    out = {}

    out['dot_number'] = dict_fmcsa['dot_number']
    out['mc_number'] = dict_mc['mc_number']
    out['mc_add_date'] = dict_mc['mc_add_date']

    return out

def check_equal(dict_mc, dict_fmcsa):

    if  dict_mc['phy_state'] == dict_fmcsa['phy_state'] and dict_mc['phy_city'] == dict_fmcsa['phy_city']:

        return True
    
    elif dict_mc['phy_state'] == dict_fmcsa['mailing_state'] and dict_mc['phy_city'] == dict_fmcsa['mailing_city']:

        return True
    
    else:
        return False




def filt(line_in):

    m = cur_line

    if line_in['legal_name'] == m:
        return True
    else:
        return False



def finder_updater():

    p = Pool(processes=3)

    global finder_updater_list
     
    finder_updater_list = list(map(clean_text, finder_updater_list))

    finder_updater_list = sorted(finder_updater_list, key= lambda i:i['legal_name'])

    global cur_line






    for i in finder_updater_list:

        cur_line = i['legal_name']

        #filtered_list = list(p.imap(filt, sorted_list_fmcsa, chunksize=3))

        filtered_list = pool_filter(p, filt, sorted_list_fmcsa)

        p.close()

        if len(filtered_list) > 0:

            for m in filtered_list:

                if check_equal(i,m):

                    print('found a detail here')
                    
                    mapper = produce_map(i,m)

                    print(mapper)

                    updater_list_to_db.append(mapper)


                    #updater_list_to_db.append(preped_class)
                    try:

                        finder_updater_list.remove(i)

                        sorted_list_fmcsa.remove(m)
                    except Exception as e:

                        print('Delete failed')

                        print(e)




        #print('in i                          %s,' % (i))

        #print(i['legal_name'][0])

        # for m in sorted_list_fmcsa:




        #     if i['legal_name'] == m['legal_name']:

        #         if check_equal(i,m):

        #             print('found a detail here')
                    
        #             mapper = produce_map(i,m)

        #             print(mapper)

        #             updater_list_to_db.append(mapper)


        #             #updater_list_to_db.append(preped_class)
        #             try:

        #                 finder_updater_list.remove(i)

        #                 sorted_list_fmcsa.remove(m)
        #             except Exception as e:

        #                 print('Delete failed')

        #                 print(e)


    do_update_dot()
    try:

        skipped_you()
    except Exception as e:

        print(e)






    print('Search finished')



def diff_list(list1, list2):

    name_mc_not_looped = []

    name_mc_looped = []

    


    for item in  list2:

        line_to_append = item['mc_number'] + '****'+item['legal_name']
        name_mc_looped.append(line_to_append)

    for item in  list1:

        line_to_append_ = item['mc_number'] + '****' + item['legal_name']
        name_mc_not_looped.append(line_to_append_)




    # set_1 = set(map(tuple, list1))
    # set_2 = set(map(tuple, list2))

    return list(set(name_mc_not_looped).symmetric_difference(set(name_mc_looped)))




def skipped_you():

    global finder_updater_list_not_looped
    global finder_updater_list

    global skipped_mc_file_name

    finder_updater_list_not_looped = list(map(clean_text, finder_updater_list_not_looped))

    finder_updater_list_not_looped = sorted(finder_updater_list_not_looped, key= lambda i:i['legal_name'])


    skipped_mc_path = os.path.join(DATA_DIR, 'mc_ops', skipped_mc_file_name )
    #[item for item in temp1 if item not in temp2]
    
    msg = diff_list(finder_updater_list_not_looped, finder_updater_list)


    with open (skipped_mc_path, 'a+') as skiped:

        #skiped_file = csv.writer(skiped, delimiter=',')

        for item in msg:
            skiped.write("%s\n" % item)



    print (len(finder_updater_list), len(finder_updater_list_not_looped))

    print('skipped you     {}'. format(len(msg)))


    finder_updater_list = []#deque([])
    finder_updater_list_not_looped = []#deque([])


def do_save_mcs():
    #data_to_db = load_all_obj(PICKLE_DB)
    global objects_to_db
    global CURRENT_LINE
    global START_TIME
    #global s

    time_now = time.time()


    mgs_p = '''
                xxxxxxx{}xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                xxxxxxxxxxxx{}xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                xxxxxxxxxxxxxxxxxxxxxxxxx{}xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            ''' 

    print(mgs_p.format(START_TIME, CURRENT_LINE, time_now))

    #print(objects_to_db)

    ses = Session()

    ses.add_all(objects_to_db)


    ses.commit()

    objects_to_db = []#deque([])


    finder_updater()




def do_update_dot():
    #data_to_db = load_all_obj(PICKLE_DB)
    global updater_list_to_db
    global CURRENT_LINE
    global START_TIME
    #global s

    time_now = time.time()


    mgs_p = '''
                DOT UPDATE{}xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                xxxxxxxxxxxx{}DOT DOT DOT DOT DOT DOT DOT DOT 
                xxxxxxxxxxxxxxxxxxxxxxxxx{}xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            ''' 

    print(mgs_p.format(START_TIME, CURRENT_LINE, time_now))

    ses = Session()

    ses.bulk_update_mappings(Fmcsa_object, updater_list_to_db)


    ses.commit()

    updater_list_to_db = []#deque([])


def check_full():
    pass




def do_f(args):

    global finder_updater_list_not_looped

    global finder_updater_list


    data_in = args

    #print (data_in)

    empty_string = ""
    empty_date = '2001,JAN,01'

    uu_id = uuid.uuid4().hex

    #print (uu_id)

    # mc_object_dict = {


    # 	'mc_id' : uu_id,

    # 	'mc_number' : purge_number(data_in[1]),
        
    # 	'mc_in' : purge_string(data_in[0]),
    #     'mc_company_name'  : purge_string(data_in[3]) ,
    #     'mc_type': purge_string([2]),
    #     'mc_city' : purge_string(data_in[4]),
    #     'mc_state':purge_string(data_in[5]),
    #     'mc_grant_date': purge_date(data_in[6]),

    #     'mc_apply_date': purge_date(data_in[7]) ,
    # }



    mc_object_dict = {


    'mc_id' : uu_id,


    'mc_number' : purge_number(data_in[1]),
        
    'mc_in' : purge_string(data_in[0]),

    'mc_company_name'  : purge_string(data_in[3]) ,

    'mc_type': purge_string(data_in[2]),

    'mc_city' : purge_string(data_in[4]),

    'mc_state':purge_string(data_in[5]),

    'mc_grant_date': purge_date(data_in[7]),

    'mc_apply_date': purge_date(data_in[6]) ,



    }


    finder_dict = {

        'legal_name': purge_string(data_in[3]),
        'phy_state': purge_string(data_in[5]) ,
        'phy_city' : purge_string(data_in[4]),
        'mc_number' : purge_string(data_in[1]),
        'mc_add_date': purge_date(data_in[7])
    }

    # update_dict = {

    # }
    

    #print (finder_dict)

    mc_object_ins = Mc_object(**mc_object_dict)

    #print (mc_object_dict)

    objects_to_db.append(mc_object_ins)

    
    finder_updater_list.append(finder_dict)

    finder_updater_list_not_looped.append(finder_dict)


    if len(objects_to_db) % 100 == 0:

    

        do_save_mcs()

    


def start_p():

    #pass

    global END_TIME
    global objects_to_db
    global CURRENT_LINE

    print("main is called")

    try:
        

        with open(CSV_FILE,'r', encoding="utf8") as fo:

            fmcsa = csv.reader(fo, delimiter="," )


            for key, line in enumerate(fmcsa):
                CURRENT_LINE = key

                #print(key)

                #print(line)

            
                #print('wwwwwwwwwsssssssssscccccccccccccddddddddddds')

                luke = line

                #print(type(luke))

                #print (line)


                if len(luke) == 0:

                    pass

                    ##print ('skiping you')

                else:

                    try:
                        do_f(luke)

                    except Exception as e:

                        print('could not do_F')

                        print (e)

                        #print (luke)




                    #print(m)


                    #dot_check = m[0]

                    #check_first = check_if_exists(dot_check)

                    # if check_first:
                    # 	skipped_you(m)
                        

                    # else:
                    # # 	do_f(m)
                        

            END_TIME = time.time()

            timeops()

            #print(objects_to_db)

            do_save_mcs()
        END_TIME = time.time()
        timeops()

    except:
        print('exception')



sorted_list_fmcsa = p()

sorted_list_fmcsa = list(map(clean_text, sorted_list_fmcsa))