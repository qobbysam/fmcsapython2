import os
import sys
import time

sys.path.append("..")



from construct.params import params

#sys.path.append("./construct")

from sqlalchemy import Column, Integer, String, Boolean, Date, ForeignKey, Sequence
from sqlalchemy.ext.declarative import declarative_base 
from sqlalchemy.orm import relationship





from random import randint 

import uuid

Base = declarative_base()

class Mc_object(Base):

	__tablename__ = 'mc_object'

	mc_number = Column(Integer, primary_key=True)

	mc_in = Column(String)

	mc_company_name = Column(String)

	mc_city = Column(String)

	mc_state = Column(String)

	mc_grant_date = Column(Date)

	mc_appy_date = Column(Date)


	def __repr__(self):

		return "<Mc_object(id='%s')>" % (self.id)


