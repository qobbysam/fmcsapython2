import io
import os

from pdfminer3.converter import TextConverter
from pdfminer3.pdfinterp import PDFPageInterpreter
from pdfminer3.pdfinterp import PDFResourceManager
from pdfminer3.pdfpage import PDFPage

# from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
# from pdfminer.converter import TextConverter
# from pdfminer.layout import LAParams
# from pdfminer.pdfpage import PDFPage


BASE_DIR = os.getcwd()

PDF_DIR = os.path.join(BASE_DIR,'pdfdata', 'apr', '')

JSON_DIR = os.path.join(BASE_DIR, 'jsondata', 'apr', '')

FILE_LOCATION_PDF =  PDF_DIR + 'REGISTER20200401.pdf'

FILE_LOCATION_JSON = JSON_DIR + 'REGISTER20200401.txt'



def extract_text_from_pdf(LOCATION_PDF):
	resource_manager = PDFResourceManager()
	fake_file_handle = io.StringIO()
	converter = TextConverter(resource_manager, fake_file_handle)
	page_interpreter = PDFPageInterpreter(resource_manager, converter)


	with open(LOCATION_PDF, 'rb') as fh:
		for page in PDFPage.get_pages(fh, caching= True, check_extractable=True):
			page_interpreter.process_page(page)

		text = fake_file_handle.getvalue()




		with open(FILE_LOCATION_JSON, 'w') as tf:
			tf.write(text)

		converter.close()
		fake_file_handle.close()

		if text:
			return text


if __name__ == '__main__':
	print(extract_text_from_pdf(FILE_LOCATION_PDF))